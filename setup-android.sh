#!/usr/bin/env bash

cd ~/
mkdir -p android/sdk
cd android/sdk
wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip platform-tools-latest-linux.zip && rm platform-tools-latest-linux.xip
wget https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip
unzip sdk-tools-linux-3859397.zip && rm sdk-tools-linux-3859397.zip

$ANDROID_HOME/tools/bin/sdkmanager --licenses
