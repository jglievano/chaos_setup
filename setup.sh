#!/usr/bin/env bash

CLOUD_DRIVE_PATH=${HOME}/Dropbox

stow_to () {
  declare dst="$1" pkg="$2"
  echo "Installing ${pkg:?} to ${dst:?}"
  stow --verbose=1 -t "${dst:?}" "$pkg"
}

GO_SYSTEM=
GO_VERSION="1.12.4"
RUBY_VERSION="2.6.3"
NVM_VERSION="0.34.0"
NODE_CUR_VERSION="12.0.0"
NODE_LTS_VERSION="10.15.3"

# Single user Nix installation.
## Someday, this will all be managed by Nix.
sh <(curl https://nixos.org/nix/install) --no-daemon

[[ -d "${CLOUD_DRIVE_PATH}" ]] || (
  printf "Cloud drive needs to be setup before system configuration."
  exit 1
)

# Setup SSH.
[[ -d "${HOME}/.ssh" ]] || mkdir ~/.ssh

cp "${CLOUD_DRIVE_PATH}/Keys/id_rsa.pub" ~/.ssh/
cp "${CLOUD_DRIVE_PATH}/Keys/id_rsa" ~/.ssh/

chmod 0700 ~/.ssh/
chmod 0644 ~/.ssh/id_rsa.pub
chmod 0600 ~/.ssh/id_rsa

# Setup Chaos file system.
mkdir ~/chaos
mkdir ~/chaos/{b,c,d,s,t}

case "${OSTYPE}" in
  linux*)
    echo "Setting up for Linux."
    ISLINUX=1
    GO_SYSTEM="linux"
    sudo apt-get update
    sudo apt-get install build-essential cmake curl fd-find git git-core \
      libcurl4-openssl-dev libffi-dev libsqlite3-dev libreadline-dev \
      libssl-dev libxml2-dev libxslt1-dev libyaml-dev nodejs \
      software-properties-common shellcheck sqlite3 stow tmux vim yarn \
      zlib1g-dev -y
    ;;
  darwin*)
    echo "Setting up for macOS."
    GO_SYSTEM="darwin"
    cd ~/ || exit
    /usr/bin/ruby -e \
      "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    # TODO: custom homebrew installation should be optional.
    # mkdir .homebrew && \
    #   curl -L https://github.com/Homebrew/brew/tarball/master \
    #   | tar xz --strip 1 -C .homebrew
    brew install bash cmake curl fd fzf git go hugo neovim openssl python \
      reattach-to-user-namespace shellcheck sqlite stow the_silver_searcher \
      tmux tree vim yarn
    ;;
  *)
    echo "unknown: ${OSTYPE}"
    exit 1
    ;;
esac

mkdir -p ~/chaos/s/jglievano/

JGL_DIR="${HOME}/chaos/s/jglievano"

# Clone chaos_utils repo
[[ -d "${JGL_DIR}/chaos_utils" ]] || \
  git clone git@gitlab.com:jglievano/chaos_setup.git \
  "${JGL_DIR}/chaos_setup"

# Clone dotfiles repo
[[ -d "${JGL_DIR}/dotfiles" ]] || \
  git clone git@gitlab.com:jglievano/dotfiles.git \
  "${JGL_DIR}/dotfiles"

[[ -e "${HOME}/.bashrc" ]] && mv ~/.bashrc ~/chaos/t/bashrc.bak
cd "${JGL_DIR}/dotfiles" || exit
stow_to "${HOME}" bin
stow_to "${HOME}" bash
stow_to "${HOME}" git
stow_to "${HOME}" vim
stow_to "${HOME}" tmux

cd || exit

ln -s .bashrc .profile
ln -s .bashrc .bash_profile

cp "${CLOUD_DRIVE_PATH}/chaos.cfg" ~/chaos/e/chaos.cfg

# --- Tools setup -------------------------------------------------------------

# Rbenv
if ! [ -x "$(command -v rbenv >/dev/null 2>&1)" ]; then
  printf "rbenv missing. Installing."
  [[ -d "${HOME}/.rbenv" ]] || git clone https://github.com/rbenv/rbenv.git ~/.rbenv
  [[ -d "${HOME}/.rbenv/plugins/ruby-build" ]] || \
    git clone https://github.com/rbenv/ruby-build.git \
    ~/.rbenv/plugins/ruby-build
  # shellcheck source=/dev/null
  source ~/.bashrc
fi

# TODO: Check if Ruby with version is being used.
# Ruby
rbenv install "${RUBY_VERSION}"
rbenv global "${RUBY_VERSION}"

# Tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# Tmuxinator
command -v tmuxinator >/dev/null 2>&1 || (
  printf "Installing Tmuxinator."
  gem install tmuxinator >/dev/null 2>&1
)
rbenv rehash

# Rust
command -v rustc >/dev/null 2>&1 || (
  printf "Installing Rust."
  curl https://sh.rustup.rs -sSf | sh >/dev/null 2>&1
)

# Go
command -v go >/dev/null 2>&1 || (
  printf "Installing Go."
  wget https://dl.google.com/go/go${GO_VERSION}.${GO_SYSTEM}-amd64.tar.gz >/dev/null 2>&1
  tar -xvf go${GO_VERSION}.${GO_SYSTEM}-amd64.tar.gz >/dev/null 2>&1
  sudo mv go /usr/local
)

# NVM
[[ -d "${HOME}/.nvm" ]] || mkdir ~/.nvm
command -v nvm >/dev/null 2>&1 || (
  printf "Installing Nvm."
  curl -o- \
    https://raw.githubusercontent.com/creationix/nvm/v${NVM_VERSION}/install.sh \
    | bash >/dev/null 2>&1
)

# shellcheck source=/dev/null
source ~/.bashrc

# Update Rust, Go, NVM installations

# Node
nvm install $NODE_LTS_VERSION
nvm install $NODE_CUR_VERSION
nvm use $NODE_CUR_VERSION

# npm, pip, go and rust installations
npm i -g csslint dockerfile_lint eslint fixjson htmlhint jsonlint \
    prettier prettier-eslint-cli prettier-standard sass-lint scss-lint \
    stylelint typescript typescript-language-server
pip3 install --upgrade autopep8 python-language-server vim-vint
go get github.com/mrtazz/checkmake
go get github.com/ckaznocha/protoc-gen-lint
go get github.com/jackc/sqlfmt/...
go get -u github.com/saibing/bingo
rustup update
rustup component add rustfmt --toolchain nightly
rustup component add rls rust-analysis rust-src

# Exa
if ! [ -x "$(command -v exa >/dev/null 2>&1)" ]; then
  printf "Installing Exa."
  cargo install exa >/dev/null 2>&1
fi

# Vim
[[ -e "${HOME}/.vimrc" ]] || ln -s ~/.vim/vimrc.vim ~/.vimrc
vim +PlugInstall +qall

# Git
[[ -e "${HOME}/.local.git" ]] || (
  read -r -p "Git user name: " git_name
  read -r -p "Git email: " git_email
  printf "[user]\n\tname = %s\n\temail = %s\n" "$git_name" "$git_email" \
      > ~/.local.git
)

# Fonts
[[ -n "${ISLINUX}" ]] && (
  sudo apt-get install xfonts-terminus
  sudo apt-get install fonts-font-awesome
)
