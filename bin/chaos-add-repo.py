#!/usr/bin/env python3

import argparse
import configparser
import os
import subprocess

class Chaos:
    DEFAULT = 'DEFAULT'
    HOSTNAME = 'hostname'
    USERNAME = 'username'
    ISGO = 'isgo'

    def __init__(self, path):
        """ Chaos constructor. Takes PATH to indicate config path. """
        self.config_path = path
        self.config = self._get_config(path)
        self.chaos_path = self.config[self.DEFAULT]['chaos'].replace('~', os.environ['HOME'])

    def add_target(self, target):
        """ Adds target. """
        print('Adding %s' % target)
        if target in self.config:
            print('git clone %s %s' % (self.get_target_url(target),
                self.get_target_path(target)))
            subprocess.run(['git', 'clone', self.get_target_url(target),
                self.get_target_path(target)])
            if self.config[target][self.ISGO]:
                print('ln -s %s %s' % (
                    self.get_target_path(target),
                    self.get_target_go_path(target)))
                subprocess.run(['mkdir', '-pv', self.get_target_go_path(target)])
                subprocess.run(['ln', '-s',
                    self.get_target_path(target),
                    self.get_target_go_path(target)])
        else:
            print('Unhandled case; %s not found in config.' % target)

    def get_target_url(self, target):
        """ Returns the URL to the target's repository. """
        return 'git@%s:%s/%s.git' % (self.config[target][self.HOSTNAME],
            self.config[target][self.USERNAME], target)

    def get_target_go_path(self, target):
        """ Returns the go path to the target."""
        return '%s/src/%s/%s' % (
            os.environ['GOPATH'],
            self.config[target][self.HOSTNAME],
            self.config[target][self.USERNAME])

    def get_target_path(self, target):
        """ Returns the local path to the target's repository. """
        return '%s/src/%s/%s' % (self.chaos_path,
            self.config[target][self.USERNAME], target)

    def _get_config(self, path):
        """ Returns a ConfigParser object with config information. """
        config_str = '[%s]\n%s' % (self.DEFAULT, open(path, 'r').read())
        config_parser = configparser.ConfigParser()
        config_parser.read_string(config_str) 
        return config_parser

def main():
    """ Entry point. """
    home_path = os.environ['HOME']
    config_path = '%s/chaos/etc/chaos.cfg' % home_path
    parser = argparse.ArgumentParser()
    parser.add_argument('target', help='Required repository.')
    parser.add_argument('--config_path', '-c', default=config_path,
        help='Path to Chaos configuration file.')
    args = parser.parse_args()
    chaos = Chaos(args.config_path)
    chaos.add_target(args.target)

if __name__ == '__main__':
    main()
