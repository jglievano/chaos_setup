# Chaos Setup

A script to setup the **Chaos** system, *and perhaps other setup utilities*.


## Usage

```bash
curl -o- https://gitlab.com/jglievano/chaos_setup/raw/master/setup.sh | bash
```

## *Chaos* setup tools

These are the packages that *Chaos* will install and setup on every machine.

- bazel
- cargo
- dmenu
- git
- nextcloud-cli
- node
- nvm
- rbenv
- rofi
- ruby
- rust
- snap
- swift
- tmux
- tmuxinator
- vim

## *Chaos* config file template

```
chaos = ~/chaos

[target1]
host = githost.com
user = gituser

[target2]
host = githost.net
user = anothergituser
```

